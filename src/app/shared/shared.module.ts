import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from './material/material.module';
import {MessagingService} from './messaging.service';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[
    MaterialModule
  ],
  providers: [MessagingService]
})
export class SharedModule { }
