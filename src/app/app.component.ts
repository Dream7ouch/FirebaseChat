import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {ChatService} from './chat/shared/providers/chat.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(public _chatService: ChatService, private router: Router) {
    this._chatService.loadUser().subscribe(user => {
      if (user && user.uid) this.router.navigate(['/chat']);
      else this.router.navigate(['/login']);
    });
  }

  logout() {
    this._chatService.logout().then(() => {
      this.router.navigate(['login']);
    });
  }

}
