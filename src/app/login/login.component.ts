import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {ChatService} from '../chat/shared/providers/chat.service';
import {AuthTypes} from '../chat/shared/enums/auth-types.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public authTypes = AuthTypes;

  constructor(private _chatService: ChatService, private router: Router) { }

  ngOnInit() {}

  login(provider: number) {
    this._chatService.login(provider).then(() => {
      this.router.navigate(['/chat']);
    });
  }

}
