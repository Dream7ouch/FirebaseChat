import {User} from './user';

export class Message {
  from: User;
  recipientId: string;
  content: string;
  date?: number;
}
