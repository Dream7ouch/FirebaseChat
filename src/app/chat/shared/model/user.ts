export class User {
  uid?: string;
  name?: string;
  avatar?: string;
  fcmTokens?: { [token: string]: true };
}
