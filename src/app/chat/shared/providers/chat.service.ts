import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';

import {AuthTypes} from '../enums/auth-types.enum';
import {Message} from '../model/message';
import {User} from '../model/user';
import {MessagingService} from '../../../shared/messaging.service';

@Injectable()
export class ChatService {

  private itemsCollection: AngularFirestoreCollection<Message>;
  messages: Message[];
  user: User = {};

  constructor(private fireStore: AngularFirestore, private afAuth: AngularFireAuth, public msg: MessagingService) {
    this.loadUser().subscribe(user => {
      if (!user) {
        this.user = {};
        return;
      }
      this.user.uid = user.uid;
      this.user.name = user.displayName;
      this.user.avatar = user.photoURL;
      this.newUser();
    });
  }

  login(provider: number) {
    let auth;
    switch (provider) {
      case AuthTypes.GOOGLE:
        auth = new firebase.auth.GoogleAuthProvider();
        break;
      case AuthTypes.TWITTER:
        auth = new firebase.auth.TwitterAuthProvider();
        break;
    }
    return this.afAuth.auth.signInWithPopup(auth);
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  notification(user) {
    this.msg.getPermission(user);
    this.msg.monitorRefresh(user);
    this.msg.receiveMessages();
  }

  async newUser() {
    const userRef = await this.fireStore.collection<User>('users').ref.where('uid', '==', this.user.uid).get();
    if (userRef.empty) await this.fireStore.doc<User>(`users/${this.user.uid}`).set(this.user);
    this.notification(this.user);
  }

  loadUser() {
    return this.afAuth.authState;
  }

  loadMessages() {
    this.itemsCollection = this.fireStore.collection<Message>('messages', ref => ref.orderBy('date', 'desc').limit(20));
    return this.itemsCollection.valueChanges().map((messages: Message[]) => {
      this.messages = messages.sort((actual, next) => actual.date - next.date);
    });
  }

  sendMessage(content: string) {
    const message: Message = {
      from: this.user,
      content,
      date: new Date().getTime(),
      recipientId: this.user.uid == '8swGZjXxhyR4LKwtRo8828pmiOC3' ? 'nQqrnT1OuDYsH3wd6xvJWfvbljm2' : this.user.uid
    };

    return this.itemsCollection.add(message);
  }

}
