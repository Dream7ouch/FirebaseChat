import {AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {MatList, MatListItem} from '@angular/material';

import {ChatService} from './shared/providers/chat.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewInit {
  @ViewChild(MatList, { read: ElementRef }) matList: ElementRef;
  @ViewChildren(MatListItem, { read: ElementRef }) matListItems: QueryList<MatListItem>;
  messageContent: string;

  constructor(public _chatService: ChatService, private router: Router) {
    this._chatService.loadMessages().subscribe(() => {
      this.scrollToBottom();
    });
  }

  ngOnInit() {
    if (!this._chatService.user.uid) this.router.navigate(['/login']);
  }

  ngAfterViewInit(): void {
    this.matListItems.changes.subscribe(() => {
      this.scrollToBottom();
    });
  }

  private scrollToBottom(): void {
    try {
      this.matList.nativeElement.scrollTop = this.matList.nativeElement.scrollHeight;
    } catch (err) {
    }
  }

  sendMessage() {
    if (!this.messageContent.length) return;
    this._chatService.sendMessage(this.messageContent);
    this.messageContent = '';
  }

}
