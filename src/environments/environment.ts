// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAexmlM83QjtVWxA4Zp2VTOG913cRH8v4g',
    authDomain: 'firechat-7dd78.firebaseapp.com',
    databaseURL: 'https://firechat-7dd78.firebaseio.com',
    projectId: 'firechat-7dd78',
    storageBucket: 'firechat-7dd78.appspot.com',
    messagingSenderId: '565201146562'
  }
};
